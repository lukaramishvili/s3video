<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CheckPosts extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'post:check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $s3      = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'post/'
        ));

        $obj_arr = $objects->toArray();

        foreach ($obj_arr as $object)
        {
            if (strpos($object['Key'], 'mp4') !== false)
            {
                $video_path = public_path() . '/' . $object['Key'];
                if (is_file($video_path))
                {
                    if (round(filesize($video_path)) == $object['Size'])
                    {
                        $this->info($video_path . ' - ' . round(filesize($video_path)) . '/' . $object['Size']);
                    }
                    else
                    {
                        $this->error($video_path . ' - ' . round(filesize($video_path)) . '/' . $object['Size']);
                    }
                }
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
