<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SyncVideo extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'video:sync';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sync videos';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
	  $this->call('video:download');
	  $this->call('video:check', [
				      '--change' => 'true'
				      ]);
          //moved to root user's crontab, nginx couldn't do it
	  //$this->call('video:generatecontactsheets');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
