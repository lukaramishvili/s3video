<?php namespace App\Console\Commands;

use Guzzle\Http\EntityBody;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CheckVideos extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'video:check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check downloaded video sizes.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $change  = $this->option('change');

        $s3      = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'videos/'
        ));

        $obj_arr = $objects->toArray();
        foreach ($obj_arr as $object)
        {
            if (strpos($object['Key'], 'mp4') !== false)
            {
                $video_path = public_path() . '/' . $object['Key'];
                if (is_file($video_path))
                {
                    if (round(filesize($video_path)) == $object['Size'])
                    {
                        $this->info($video_path . ' - ' . round(filesize($video_path)) . '/' . $object['Size']);
                    }
                    else
                    {
                        $this->error($video_path . ' - ' . round(filesize($video_path)) . '/' . $object['Size']);
                        if ($change == 'true')
                        {
                            $this->info('Downloading...');

                            $s3->getObject([
                                'Bucket'                => 'gdsonvideos1',
                                'Key'                   => $object['Key'],
                                'command.response_body' => EntityBody::factory(fopen(public_path() . '/' . $object['Key'] , 'w+'))
                            ]);

                            $this->info($object['Key'] . ' download complete');
                        }
                    }
                }
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['change', null, InputOption::VALUE_OPTIONAL, 'Download not fully downloaded videos', null],
		];
	}

}
