<?php namespace App\Console\Commands;

use Guzzle\Http\EntityBody;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\Response;

class DownloadVideos extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'video:download';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Download videos from amazon';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $video_id   = $this->argument('video_id');
        $video_name = $this->argument('video_name');

        if (isset($video_id) && !empty($video_id))
        {
            if (isset($video_name) && !empty($video_name))
            {
                //get video by id and name
                $this->video($video_id, $video_name);
            }
            else
            {
                //get videos from video objec folder
                $this->videosFromFolder($video_id);
            }
        }
        else
        {
            //get all videos function
            $this->allVideos();
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['video_id', InputArgument::OPTIONAL, 'Video id'],
			['video_name', InputArgument::OPTIONAL, 'Video name'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

    /**
     * Download all videos
     *
     * @return Response
     */
    private function allVideos()
    {
        $s3      = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'videos/'
        ));

        $obj_arr = $objects->toArray();
        array_shift($obj_arr);

        $result = [];
        foreach ($obj_arr as $object)
        {
            if (strpos($object['Key'], 'mp4') !== false)
            {
                $key = trim(str_replace('/1280x720.mp4', '', $object['Key']));
                $key = trim(str_replace('/1920x1080.mp4', '', $key));
                $key = trim(str_replace('/352x240.mp4', '', $key));
                $key = trim(str_replace('/480x360.mp4', '', $key));
                $key = trim(str_replace('/858x480.mp4', '', $key));

                $video_path = public_path() . '/' . $object['Key'];
                if (!file_exists($video_path))
                {
                    $this->info('Downloading... '. $object['Key']);
                    $dir = public_path() . '/' . $key;
                    if (!is_dir($dir))
                    {
                        mkdir($dir, 0755, true);
                    }

                    $result[] = $s3->getObject([
                        'Bucket'                => 'gdsonvideos1',
                        'Key'                   => $object['Key'],
                        'command.response_body' => EntityBody::factory(fopen(public_path() . '/' . $object['Key'], 'w+'))
                    ]);

                    $this->info($object['Key'] . ' download complete');
                }
                else
                {
                    $this->error('File ' . $object['Key'] . ' already exists!');
                }
            }
        }

        $this->info('Download Complete');
    }

    /**
     * Download one video
     *
     * @param $video_id
     * @param $video_name
     */
    private function video($video_id, $video_name)
    {
        $s3  = App::make('aws')->get('s3');

        $video_path = public_path() . '/videos/' . $video_id . '/' . $video_name;
        if (!file_exists($video_path))
        {
            $this->info('Downloading... ');

            $dir = public_path() . '/videos/' . $video_id;
            if (!is_dir($dir))
            {
                mkdir($dir, 0755, true);
            }

            $s3->getObject([
                'Bucket'                => 'gdsonvideos1',
                'Key'                   => 'videos/' . $video_id . '/' . $video_name,
                'command.response_body' => EntityBody::factory(fopen($dir . '/' . $video_name , 'w+'))
            ]);

            $this->info('videos/' . $video_id . '/' . $video_name . ' download complete');
        }
        else
        {
            $this->error('File videos/' . $video_id . '/' . $video_name . ' already exists!');
        }
    }

    /**
     * Download all videos from video object folder
     *
     * @param $video_id
     */
    private function videosFromFolder($video_id)
    {
        $s3 = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'videos/' . $video_id . '/'
        ));

        $obj_arr = $objects->toArray();

        $result = [];
        foreach ($obj_arr as $object) {
            if (strpos($object['Key'], 'mp4') !== false) {
                $key = trim(str_replace('/1280x720.mp4', '', $object['Key']));
                $key = trim(str_replace('/1920x1080.mp4', '', $key));
                $key = trim(str_replace('/352x240.mp4', '', $key));
                $key = trim(str_replace('/480x360.mp4', '', $key));
                $key = trim(str_replace('/858x480.mp4', '', $key));

                $video_path = public_path() . '/' . $object['Key'];
                if (!file_exists($video_path)) {
                    $this->info('Downloading... ' . $object['Key']);

                    $dir = public_path() . '/' . $key;
                    if (!is_dir($dir)) {
                        mkdir($dir, 0755, true);
                    }

                    $result[] = $s3->getObject([
                        'Bucket' => 'gdsonvideos1',
                        'Key' => $object['Key'],
                        'command.response_body' => EntityBody::factory(fopen(public_path() . '/' . $object['Key'], 'w+'))
                    ]);

                    $this->info($object['Key'] . ' download complete');
                } else {
                    $this->error('File ' . $object['Key'] . ' already exists!');
                }
            }
        }
    }
}
