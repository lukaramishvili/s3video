<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class GenerateContactSheets extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'video:generatecontactsheets';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate contact sheets for videos';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
	  $video_id = $this->argument('video_id');
	  if(isset($video_id) && !empty($video_id) && is_numeric($video_id)){
	    $this->generate_contactsheet_for_one_video($video_id);
	  } else {
	    $videos = \File::directories(public_path().'/videos/');
	    foreach($videos as $vdir){
	      $current_video_id = \File::name($vdir);
	      $this->generate_contactsheet_for_one_video($current_video_id);
	    }
	  }
	}

	private function generate_contactsheet_for_one_video($video_id){
	  $vdir = public_path().'/videos/'.$video_id;
	  if(\File::exists($vdir.'/858x480.mp4') && (false == \File::exists($vdir.'/contactsheet.jpg'))){
	    //$process = new Process('/usr/local/bin/ffmpeg -loglevel panic -y -i "'.$vdir.'/858x480.mp4" -frames 1 -q:v 12 -vf "select=not(mod(n\,250)),scale=115:-1,tile=64x64" '.$vdir.'/contactsheet.jpg');
	    $frames_dir = $vdir.'/frames';
	    if (!is_dir($frames_dir)){
	      mkdir($frames_dir, 0755, true);
	    }
	    // (cpulimit -l 80 ffmpeg..) &&
	    $process = new Process('/usr/local/bin/ffmpeg -loglevel panic -y -i "'.$vdir.'/858x480.mp4" -q:v 12 -vf "fps=1/10,scale=115:-1" '.$frames_dir.'/frame%03d.jpg '
				   .' && montage '.$frames_dir.'/frame*.jpg -tile 64x64 -geometry 115x65 -background black '.$vdir.'/contactsheet.jpg'
				   .' && chown -R nginx frames contactsheet.jpg && chmod -R 0775 frames contactsheet.jpg');
	    $process->setTimeout(20 * 60);
	    $process->setIdleTimeout(20 * 60);
	    //$process->start();//start is async and resulted in all the videos worked in parallel
	    $process->run();//run the process in blocking mode
	  }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			['video_id', InputArgument::OPTIONAL, 'Video id']
			];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
