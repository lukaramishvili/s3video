<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
        'App\Console\Commands\DownloadVideos',
        'App\Console\Commands\PostVideoDownload',
        'App\Console\Commands\CheckVideos',
        'App\Console\Commands\CheckPosts',
        'App\Console\Commands\SyncVideo',
        'App\Console\Commands\GenerateContactSheets'
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();

        $schedule->command('video:sync')
                 ->withoutOverlapping()
                 ->everyTenMinutes();
	         //->after(function () {
		 //    $schedule->exec('video:generatecontactsheets');
		 //  });
	}

}
