<?php namespace App\Http\Controllers;

use Guzzle\Http\EntityBody;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Download all videos
	 *
	 * @return Response
	 */
	public function index()
	{
        exit;
        $s3      = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'videos/'
        ));

        $obj_arr = $objects->toArray();
        array_shift($obj_arr);

        $result = [];
        foreach ($obj_arr as $object)
        {
            if (strpos($object['Key'], 'mp4') !== false)
            {
                $key = trim(str_replace('/1280x720.mp4', '', $object['Key']));
                $key = trim(str_replace('/1920x1080.mp4', '', $key));
                $key = trim(str_replace('/352x240.mp4', '', $key));
                $key = trim(str_replace('/480x360.mp4', '', $key));
                $key = trim(str_replace('/858x480.mp4', '', $key));

                $video_path = public_path() . '/' . $object['Key'];
                if (!is_dir($video_path))
                {
                    $dir = public_path() . '/' . $key;
                    if (!is_dir($dir))
                    {
                        mkdir($dir, 0755, true);
                    }

                    $result[] = $s3->getObject([
                        'Bucket'                => 'gdsonvideos1',
                        'Key'                   => $object['Key'],
                        'command.response_body' => EntityBody::factory(fopen(public_path() . '/' . $object['Key'], 'w+'))
                    ]);
                }

                print public_path() . '/' . $object['Key'] . '<br>';
            }
        }

        echo '<pre>';
        print_r($result);
        exit;
	}

    /**
     * Download one video
     *
     * @param $video_id
     * @param $video_name
     */
    public function video($video_id, $video_name)
    {
        $s3  = App::make('aws')->get('s3');

        $dir = public_path() . '/videos/' . $video_id;
        if (!is_dir($dir))
        {
            mkdir($dir, 0755, true);
        }

        $result = $s3->getObject([
            'Bucket'                => 'gdsonvideos1',
            'Key'                   => 'videos/' . $video_id . '/' . $video_name,
            'command.response_body' => EntityBody::factory(fopen($dir . '/' . $video_name , 'w+'))
        ]);

        echo '<pre>';
        print_r($result->toArray());
        exit;
    }

    /**
     * Download all videos from video object folder
     *
     * @param $video_id
     */
    public function videosFromFolder($video_id)
    {
        $s3      = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'videos/' . $video_id . '/'
        ));

        $obj_arr = $objects->toArray();

        $result = [];
        foreach ($obj_arr as $object)
        {
            if (strpos($object['Key'], 'mp4') !== false)
            {
                $key = trim(str_replace('/1280x720.mp4', '', $object['Key']));
                $key = trim(str_replace('/1920x1080.mp4', '', $key));
                $key = trim(str_replace('/352x240.mp4', '', $key));
                $key = trim(str_replace('/480x360.mp4', '', $key));
                $key = trim(str_replace('/858x480.mp4', '', $key));

                $video_path = public_path() . '/' . $object['Key'];
                if (!is_dir($video_path))
                {
                    $dir = public_path() . '/' . $key;
                    if (!is_dir($dir))
                    {
                        mkdir($dir, 0755, true);
                    }

                    $result[] = $s3->getObject([
                        'Bucket'                => 'gdsonvideos1',
                        'Key'                   => $object['Key'],
                        'command.response_body' => EntityBody::factory(fopen(public_path() . '/' . $object['Key'], 'w+'))
                    ]);
                }
            }
        }
    }

    /**
     * Check video size
     */
    public function checkVideo()
    {
        $s3      = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'videos/'
        ));

        $obj_arr = $objects->toArray();

        foreach ($obj_arr as $object)
        {
            if (strpos($object['Key'], 'mp4') !== false)
            {
                $video_path = public_path() . '/' . $object['Key'];
                if (is_file($video_path))
                {
                    if (round(filesize($video_path)) != $object['Size'])
                    {
                        print $video_path . ' - ' . round(filesize($video_path)) . '/' . $object['Size'] . '<br>';
                    }
                }
            }
        }
    }

    /**
     * Check post video size
     */
    public function checkPost()
    {
        $s3      = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => 'gdsonvideos1',
            'Prefix' => 'post/'
        ));

        $obj_arr = $objects->toArray();

        foreach ($obj_arr as $object)
        {
            if (strpos($object['Key'], 'mp4') !== false)
            {
                $video_path = public_path() . '/' . $object['Key'];
                if (is_file($video_path))
                {
                    if (round(filesize($video_path)) != $object['Size'])
                    {
                        print $video_path . ' - ' . round(filesize($video_path)) . '/' . $object['Size'] . '<br>';
                    }
                }
            }
        }
    }

}
