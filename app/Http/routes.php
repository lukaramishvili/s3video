<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/video/{video_id}', 'WelcomeController@videosFromFolder');
Route::get('/video/{video_id}/{video_name}', 'WelcomeController@video');

Route::get('/checkVideo', 'WelcomeController@checkVideo');
Route::get('/checkPost', 'WelcomeController@checkPost');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
